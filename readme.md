# [keycloak-vidlets](https://bitbucket.org/javapda/keycloak-vidlets)

* [keycloak-vidlets youtube channel](https://www.youtube.com/channel/UCs9hJMAt25l3wDIQ9rgEkmw)

* [How do you find the keycloak version?](./vidlets/How%20do%20you%20find%20the%20keycloak%20version?/readme.md)
* [How to change keycloak login theme from the command-line?](vidlets/How%20to%20change%20keycloak%20login%20theme%20from%20the%20command-line?/readme.md)
* [How to list json output about available realms?](vidlets/How%20to%20list%20json%20output%20about%20available%20realms?/readme.md)
* [How to start keycloak on a specific port number?](vidlets/How%20to%20start%20keycloak%20on%20a%20specific%20port%20number?/readme.md)
* [How to use REST to get an access token?](vidlets/How%20to%20use%20REST%20to%20get%20an%20access%20token?/readme.md)
