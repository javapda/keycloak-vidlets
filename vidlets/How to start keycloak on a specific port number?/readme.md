

# How to start keycloak on a specific port number?

# Overview
* This keycloak-vidlet answers the question of *How can you start keycloak so that it listens on a specific port number?*

# Video
* [on youtube]()

# Steps

* Run the following command:
```
KEYCLOAK_PORT=8899 JAVA_OPTS=-Djboss.http.port=${KEYCLOAK_PORT} ${{KEYCLOAK_)HOME}/bin/standalone.sh
```

# Details

By default, keycloak will start on port _8080_. However, this can become a conflict if other servers (e.g. tomcat) are already running on _8080_.

Some environment variables:

KEYCLOAK_PORT : the desired port number (e.g. 8899)
KEYCLOAK_HOME : directory where keycloak is installed (e.g. /opt/keycloak)

The command:
```
KEYCLOAK_PORT=8899 JAVA_OPTS=-Djboss.http.port=${KEYCLOAK_PORT} ${{KEYCLOAK_)HOME}/bin/standalone.sh
```