

# How to change keycloak login theme from the command-line?

# Overview
* This keycloak-vidlet answers the question of *How do you change the login theme, for a realm, from the command-line?*

# Video
* [on youtube](https://youtu.be/0_-5lY1wVu8)

# Steps

1. Log into keycloak master realm as admin
2. Run command to set theme

## Step 1: Log into keycloak master realm as admin
```
kcadm.sh config credentials --server http://localhost:8090/auth --realm master --user admin --password admin
```

## Step 2: Run command to set theme
```
kcadm.sh update realms/master -s "loginTheme=my-demo-theme"
```

# Details

if you look in your keycloak home folder you will find a folder called _themes_. Each folder in themes folder represents an available theme.  I have keycloak version 13.0.1 installed. By default it comes with the following themes:

* base
* keycloak
* keycloak.v2


If you have developed your own custom theme it will be represented by its own folder in the ${KEYCLOAK_HOME}/themes directory.


