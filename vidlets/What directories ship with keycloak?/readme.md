# What directories ship with keycloak?

## Directories - (as of keycloak 13.0.1 - June 2021)

* bin : scripts (start or manage keycloak)
* docs : documentation
* domain : properties files (more configuration) for domain mode
* modules : module configuration and java libraries
* standalone : base configuration, logs - for standalone mode
* themes : customize appearance
* welcome-content : redirect to /auth

## more info

* [keycloak directory structures](https://www.keycloak.org/docs/latest/server_installation/index.html#distribution-directory-structure)
* [standalone mode](https://www.keycloak.org/docs/latest/server_installation/index.html#_standalone-mode)
* [domain mode](https://www.keycloak.org/docs/latest/server_installation/index.html#_domain-mode)