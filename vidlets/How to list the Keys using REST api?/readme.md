# How to list the Keys using REST api?

# commands
## get access token
```
export keycloakHost=localhost; export keycloakPort=8090; export access_token=$(curl --silent -d "client_id=admin-cli" -d "username=admin" -d "password=admin" -d "grant_type=password" "http://${keycloakHost}:${keycloakPort}/auth/realms/master/protocol/openid-connect/token" | jq --raw-output '.access_token')

```
## get keys
```
curl --silent -H  "Authorization: bearer ${access_token}" "http://localhost:8090/auth/admin/realms/master/keys"
```