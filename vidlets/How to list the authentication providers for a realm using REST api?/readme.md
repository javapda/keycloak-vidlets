# How to list the authentication providers for a realm using REST api?

# command
```
export keycloakHost=localhost; export keycloakPort=8090; export access_token=$(curl --silent -d "client_id=admin-cli" -d "username=admin" -d "password=admin" -d "grant_type=password" "http://${keycloakHost}:${keycloakPort}/auth/realms/master/protocol/openid-connect/token" | jq --raw-output '.access_token')

export realm=master;curl --silent -H  "Authorization: bearer ${access_token}" "http://${keycloakHost}:${keycloakPort}/auth/admin/realms/${realm}/authentication/authenticator-providers"
```

(Note: you will want to adjust the environment variables to match your configuration)