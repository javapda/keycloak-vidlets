# How to get keycloak running in less than 30 seconds?

* [youtube video](https://youtu.be/N3dCZ1pRwAQ)

# steps
1. download keycloak distribution
2. unpack keycloak
3. start keycloak
4. add admin user
5. restart keycloak
6. run admin client in browser

# command
```

export keycloakVersion=13.0.1;  export keycloakDir=“keycloak-${keycloakVersion}"; export keycloakGzip="${keycloakDir}.tar.gz";
export keycloakDownloadUrl="https://github.com/keycloak/keycloak/releases/download/${keycloakVersion}/${keycloakGzip}";
curl -L "https://github.com/keycloak/keycloak/releases/download/${keycloakVersion}/keycloak-${keycloakVersion}.tar.gz" --output ${keycloakGzip};
tar xzf ${keycloakGzip};
export START_TIME=${SECONDS};
cd "${keycloakDir}"
./bin/standalone.sh &;
sleep 12;
./bin/add-user-keycloak.sh --user admin --password admin -r master
pid=$(lsof -i :9990 | grep LISTEN | awk '{print $2}');
kill -9 ${pid};
sleep 2;
./bin/standalone.sh &;
sleep 12;
open http://localhost:8080/auth/admin
echo "duration: $(($SECONDS - $START_TIME)) sec"

```