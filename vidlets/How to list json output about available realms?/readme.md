# How to list json output about available realms?



# general form
kcadm.sh get realms --no-config --server http://localhost:8090/auth --realm master --user admin --password admin


# command
```
export KEYCLOAK_PORT=8090 ;
export KEYCLOAK_ADMIN_USERNAME=admin ;
export KEYCLOAK_ADMIN_PASSWORD=admin ;
${HOME}/keycloak/bin/kcadm.sh get realms --no-config --server "http://localhost:${KEYCLOAK_PORT}/auth" --realm master --user ${KEYCLOAK_ADMIN_USERNAME} --password ${KEYCLOAK_ADMIN_PASSWORD}
```

# output to file:
```
export KEYCLOAK_PORT=8090 ;
export KEYCLOAK_ADMIN_USERNAME=admin ;
export KEYCLOAK_ADMIN_PASSWORD=admin ;
${HOME}/keycloak/bin/kcadm.sh get realms --no-config --server "http://localhost:${KEYCLOAK_PORT}/auth" --realm master --user ${KEYCLOAK_ADMIN_USERNAME} --password ${KEYCLOAK_ADMIN_PASSWORD} > /tmp/myrealms.json
```