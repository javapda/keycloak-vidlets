# How to run keycloak in docker?

## this will download the keycloak docker image, start it, set admin, restart it and open browser to the keycloak installation

# Command
1. pull the docker image
2. start the docker container (wait a bit for the container to start)
3. set the admin password
4. restart docker container
```
KEYCLOAK_PORT=8091 && docker run -p ${KEYCLOAK_PORT}:8080 jboss/keycloak 

# wait a bit for the download and startup of keycloak container
docker_container_id=$(docker container ls | grep jboss/keycloak | awk '{print $1}') \
&& docker exec ${docker_container_id} /opt/jboss/keycloak/bin/add-user-keycloak.sh -u admin -p admin 

docker_container_id=$(docker container ls | grep jboss/keycloak | awk '{print $1}') \
&& docker restart "${docker_container_id}" \
&& KEYCLOAK_PORT=8091 && sleep 10 && open "http://localhost:${KEYCLOAK_PORT}/"
```


## How do you stop the docker container?
```
docker_container_id=$(docker container ls --all  | grep jboss/keycloak | awk '{print $1}') \
&& docker stop ${docker_container_id}
```
## How do you remove the docker container?
```
docker_container_id=$(docker container ls --all  | grep jboss/keycloak | awk '{print $1}') \
&& docker container rm ${docker_container_id}
```
## How do you remove the docker image?
```
docker_image_id=$(docker image ls --all  | grep jboss/keycloak | awk '{print $3}') \
&& docker image rm ${docker_image_id}
```

## How do you start the docker container?
```
docker_container_id=$(docker container ls --all | grep jboss/keycloak | awk '{print $1}');
docker start ${docker_container_id}
```

## How do you restart the docker container?
```
docker_container_id=$(docker container ls --all | grep jboss/keycloak | awk '{print $1}') \
&& docker restart ${docker_container_id}
```

## How to remove the docker image?
1. stop the container
2. remove the container
3. remove the image
```
docker_container_id=$(docker container ls --all | grep jboss/keycloak | awk '{print $1}') \
&& docker stop ${docker_container_id} \
&& docker container rm ${docker_container_id}

docker_image_id=$(docker images | grep jboss/keycloak | awk '{print $3}') \
&& docker rmi -f ${docker_image_id}
```