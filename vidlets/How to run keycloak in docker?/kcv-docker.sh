#!/usr/bin/env bash
############################################################
# PURPOSE: keycloak and docker
############################################################
# . $(dirname "${0}")/lib.sh

commands=()
keycloakPort=8091
_isDockerInstalled() {
    if $(which docker > /dev/null); then
        true
    else
        false
    fi
}
_usage() {
    echo "
  ${YELLOW}USAGE${_normal}
    $_progname [options] 

  ${YELLOW}OPTIONS${_normal}
    -h, --help                         show this help
    --verbose                          more output

  ${YELLOW}EXAMPLES${_normal}
    $_progname --help

  ${YELLOW}NOTES${_normal}
    * requires docker [$(if $(_isDockerInstalled); then echo "yes installed"; else echo "no not installed"; fi)]

    "
}
############################################################
# _isMacosx
# usage:
#   _isMacosx && ...do something...
############################################################
_isMacosx() {
  if [[ "$(uname)" == 'Darwin' ]]; then
    true
  else
    false
  fi
}
############################################################
# _isCentos
# usage:
#   _isCentos && ...do something...
############################################################
_isCentos() {
  if [[ "$(uname)" == 'Linux' && -f /etc/centos-release ]]; then
    true
  else
    false
  fi
}
_isLinux() {
  if [[ $(_machine) == "Linux" ]]; then
    true
  else
    false
  fi
}
_isMac() {
  if [[ $(_machine) == "Mac" ]]; then
    true
  else
    false
  fi
}

_machine() {
  local unameOut="$(uname -s)"
  case "${unameOut}" in
      Linux*)     machine=Linux;;
      Darwin*)    machine=Mac;;
      CYGWIN*)    machine=Cygwin;;
      MINGW*)     machine=MinGw;;
      *)          machine="UNKNOWN:${unameOut}"
  esac
  echo "${machine}"
}
_showParams() {
    echo "
       *** parameters ***
       keycloakPort:          ${keycloakPort}
       verbose:               ${verbose}
       *** info ***
       machine:               $(_machine)
       hostname:              $(hostname)
    "
}
_dockerRunKeycloakBase() {
    echo "TODO: _dockerRunKeycloakBase"
    if (_isMacosx); then
        echo "MAC TIME"
  local start=${SECONDS}
        local cmd="KEYCLOAK_PORT=${keycloakPort} && docker run -p ${KEYCLOAK_PORT}:8080 jboss/keycloak "
              osascript <<END 
tell app "Terminal" to do script "${cmd}" 
END

    elif (_isCentos); then
        echo "CENTOS : not supported"
    else
        echo "unsupported machine: $(_machine)"
    fi
}
_dockerDeleteContainer() {
    read -p "enter container id: " answer
    if [[ "${answer}" != "" ]]; then
        local docker_container_id="${answer}"
        echo "removing ${docker_container_id}"
        docker container rm ${docker_container_id}
        
    fi
}
_dockerContainers() {
    docker container ls --all |tail -n+2
}
_dockerContainerStop() {
    read -p "enter container id: " answer
    if [[ "${answer}" != "" ]]; then
        local docker_container_id="${answer}"
        echo "stopping ${docker_container_id}"
        docker container stop ${docker_container_id}
    fi
}
_dockerContainerStart() {
    read -p "enter container id: " answer
    if [[ "${answer}" != "" ]]; then
        local docker_container_id="${answer}"
        echo "starting ${docker_container_id}"
        docker container start ${docker_container_id}
    fi
}
_dockerContainerRestart() {
    read -p "enter container id: " answer
    if [[ "${answer}" != "" ]]; then
        local docker_container_id="${answer}"
        echo "restarting ${docker_container_id}"
        docker container restart ${docker_container_id}
    fi
}

_dockerDeleteImage() {
    read -p "enter image id: " answer
    if [[ "${answer}" != "" ]]; then
        local docker_image_id="${answer}"
        echo "removing ${docker_image_id}"
        docker image rm ${docker_image_id}
        
    fi
}
_dockerKeycloakContainerId() {
    docker container ls --all | grep jboss\/keycloak | awk '{print $1}'
}

_dockerBashShell() {
    # local docker_keycloak_container_id=$(docker container ls --all | grep jboss\/keycloak | awk '{print $1}')
    local docker_keycloak_container_id=$(_dockerKeycloakContainerId)
    if [[ "${docker_keycloak_container_id}" != "" ]]; then
        docker exec -it ${docker_keycloak_container_id} bash
    else
        echo "ERROR: no keycloak container found"
    fi
}
_isDockerContainerRunning() {
  local container_name="$(_dockerKeycloakContainerId)"
  if [[ "$( docker container inspect -f '{{.State.Running}}' ${container_name})" == "true" ]]; then
  # if [[ "$( docker container inspect -f '{{.State.Running}}' ${container_name})" == "true" ]]; then
    true
  else
    false
  fi
}
_interactiveMode() {
  local options=()
  options+=("docker run keycloak base")
  options+=("docker keycloak container id")
  options+=("docker containers")
  options+=("docker images")
  options+=("docker stop container")
  options+=("docker start container")
  options+=("docker restart container")
  options+=("is docker container running?")

  options+=("docker delete container")
  options+=("docker delete image")
  options+=("docker bash shell")
  options+=("show params")
  options+=(help quit)
  local prompt="Select option: "
  PS3="${prompt}"
  select choice in "${options[@]}"; do
    case "${choice}" in
        "docker keycloak container id")
          echo "$(_dockerKeycloakContainerId)"
          ;;
        "is docker container running?")
            $(_isDockerContainerRunning) && echo "RUNNING [$(_dockerKeycloakContainerId)]"
            $(! _isDockerContainerRunning) && echo "NOT RUNNING [$(_dockerKeycloakContainerId)]"
            ;;

        "docker bash shell")
            _dockerBashShell
            ;;
        "docker stop container")
            _dockerContainerStop
            ;;
        "docker start container")
            _dockerContainerStart
            ;;
        "docker restart container")
            _dockerContainerRestart
            ;;
        "docker delete container")
            _dockerDeleteContainer
            ;;
        "docker delete image")
            _dockerDeleteImage
            ;;
        "docker containers")
            _dockerContainers
            ;;
        "docker images")
            docker image ls --all
            ;;
        "docker run keycloak base")
            _dockerRunKeycloakBase
            ;;
        "show params")
            _showParams
            ;;        
        help)
            _usage
            ;;
        quit)
            _quit
            ;;
        *)
            [[ "${choice}" == "" ]] && echo "invalid choice"
            ;;
    esac
  done
}


while [ $# -gt 0 ]
do
    case "$1" in
        -h|--help)
            _usage
            exit 0
            ;;

        --verbose)
            verbose=true
            ;;

        *)
            abort "Do not know how to process option '$1'"
            ;;

    esac
    shift
done
[[ -z "${verbose+x}" ]] || _showParams
if [[ "${#commands[@]}" -gt 0 ]]; then
  _processCommands
else
  _interactiveMode
fi 
