# How to use REST to get detail about master realm?



```
export keycloakPort=8090; export adminUser=admin; export adminPassword=admin; 
export access_token=$(curl --silent \
-d "client_id=admin-cli" -d "username=${adminUser}" \
-d "password=${adminPassword}" -d "grant_type=password" \
"http://localhost:${keycloakPort}/auth/realms/master/protocol/openid-connect/token" | jq --raw-output '.access_token');

export realmOfInterest=demo; 
curl --silent \
  -H "Authorization: bearer ${access_token}" \
  "http://localhost:8090/auth/admin/realms/${realmOfInterest}"
```