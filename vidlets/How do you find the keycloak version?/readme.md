# How do you find the keycloak version?

${KEYCLOAK_HOME}/bin/standalone.sh —version


# sample output
```
${KEYCLOAK_HOME}/bin/standalone.sh --version
=========================================================================

  JBoss Bootstrap Environment

  JBOSS_HOME: /Users/myuser/keycloak

  JAVA: /Library/Java/JavaVirtualMachines/jdk-15.0.2.jdk/Contents/Home/bin/java

  JAVA_OPTS:  -server -Xms64m -Xmx512m -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=256m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true  --add-exports=java.base/sun.nio.ch=ALL-UNNAMED --add-exports=jdk.unsupported/sun.misc=ALL-UNNAMED --add-exports=jdk.unsupported/sun.reflect=ALL-UNNAMED

=========================================================================

20:42:59,528 INFO  [org.jboss.modules] (main) JBoss Modules version 1.11.0.Final
Keycloak 13.0.1 (WildFly Core 15.0.1.Final)
```

# Info
* What are all those environment variables?
```
KEYCLOAK_HOME : the directory/folder where keycloak is installed
```
* What is standalone.sh?
```
the main launch script for keycloak
```
